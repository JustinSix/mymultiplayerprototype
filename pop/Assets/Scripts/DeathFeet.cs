﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFeet : MonoBehaviour
{
    public Player myPlayer;
    void Start()
    {

    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.attachedRigidbody == null) return;

        myPlayer.KillHit(col.attachedRigidbody);
    }
    void Update()
    {

    }
}
