﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    // Config
    [SerializeField] float runSpeed = 6.5f;
    [SerializeField] float jumpSpeed = 11;
    [SerializeField] string playerHori = "HorizontalP1";
    [SerializeField] string playerJump = "JumpP1";

    // State
    bool isAlive = true;

    // Cached component references 
    //Animator myAnimator;
    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myFeet;
    Rigidbody2D myRigidBody;

    // Message then methods
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
       // myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        myFeet = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Die();
        if(isAlive)
        {
            Run();
            Jump();
        }

        FlipSprite();
    }

    private void Run()
    {
        float controlThrow = CrossPlatformInputManager.GetAxis(playerHori); // value between -1 & 1
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;

        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
       // myAnimator.SetBool("Running", playerHasHorizontalSpeed);

    }

    private void Jump()
    {
       if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground", "Player"))) { return; }

       if (CrossPlatformInputManager.GetButtonDown(playerJump))
        {
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;
        }
    }

    public void KillHit(Rigidbody2D rb)
    {
        Player enemy = rb.gameObject.GetComponent<Player>();
        if (enemy != null)
        {
            enemy.Die();
        }
    }


    public void Die()
    {
        if (myFeet.IsTouchingLayers(LayerMask.GetMask("Player")))
        {
            print("DEATH" + playerHori);

            isAlive = false;

            Destroy(gameObject);
        }
    }
    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);
        }
    }
}
